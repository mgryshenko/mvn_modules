package com.mgryshenko.samples.mvn_modules;

import org.apache.log4j.AppenderSkeleton;
import org.apache.log4j.Logger;
import org.apache.log4j.spi.LoggingEvent;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.mockito.Mockito.verify;

@RunWith(MockitoJUnitRunner.class)
public class EntryPointTest {

    @Mock
    AppenderSkeleton appender;

    @Captor
    ArgumentCaptor<LoggingEvent> captor;

    @Test
    public void testHelloWorldLoggedOut() {
        Logger.getRootLogger().addAppender(appender);

        EntryPoint.main();

        verify(appender).doAppend(captor.capture());
        Assert.assertEquals("Hello world not logged", "Hello World!", captor.getValue().getRenderedMessage());
    }
}