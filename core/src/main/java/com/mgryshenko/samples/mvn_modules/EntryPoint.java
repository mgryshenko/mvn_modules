package com.mgryshenko.samples.mvn_modules;

import org.apache.log4j.Logger;

public class EntryPoint {

    private static final Logger LOG = Logger.getLogger(EntryPoint.class);

    public static void main(String... args) {
        LOG.info("Hello World!");
    }
}